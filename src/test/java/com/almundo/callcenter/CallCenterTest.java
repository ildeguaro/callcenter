package com.almundo.callcenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.almundo.callcenter.domain.CallCenter;
import com.almundo.callcenter.domain.CustomerCallingEmulator;
import com.almundo.callcenter.domain.Director;
import com.almundo.callcenter.domain.Operador;
import com.almundo.callcenter.domain.Supervisor;
import com.almundo.callcenter.exceptions.NoCallRegisteredException;
import com.almundo.callcenter.exceptions.NoEmpleyeesRegisteredExpection;
import com.almundo.callcenter.util.CallQueue;

/**
 * Clase de Testing de la Generacion de Llamadas de manera concurrente
 * y la atencio de las misma priorizando Operadores si estos
 * estan disponibles
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 */
public class CallCenterTest {
	
	private CallCenter callCenter;
	
	private CustomerCallingEmulator customerCallingEmulator;
	
	
	@Before
    public void setUp() throws Exception {
		callCenter = new CallCenter();
		callCenter.registerEmploy(3, Operador.class);
		callCenter.registerEmploy(1, Supervisor.class);
		callCenter.registerEmploy(1, Director.class);
		
		customerCallingEmulator = new CustomerCallingEmulator();		 
	}
	
	@After
    public void tearDown() throws Exception {
		callCenter.stopDispatchCall();
    }
	
	/**
	 * Imprime la salida por consola de la interaccion desde la
	 * generacion de la llamada hasta el despacho y atencion.
	 * <p>
	 * En la salida se muestra los dialogos del dispatcher y lo empleados.
	 * 
	 * Escenario: 10 llamadas generadas de manera concurrente.
	 * Se espera 35 segundos que ocurra los procesos concurrentes.
	 */
	@Test	
	public void printInteraction10CallsTest() throws NoEmpleyeesRegisteredExpection, 
														NoCallRegisteredException, 
														InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(10, callCenter);
		Thread.sleep(35000);
	}
	
	/**
	 * Imprime la salida por consola de la interaccion desde la
	 * generacion de la llamada hasta el despacho y atencion.
	 * <p>
	 * En la salida se muestra los dialogos del dispatcher y los empleados.
	 * 
	 * Escenario: Mas de 10 llamadas generadas de manera concurrente.
	 * Se espera mas de un minuto  que ocurra los procesos concurrentes.
	 */
	@Test	
	public void printInteractionMoreThan10CallsTest() throws NoEmpleyeesRegisteredExpection, 
															NoCallRegisteredException, 
															InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(20, callCenter);
		Thread.sleep(70000);
	}
	
	/**
	 * Verifica si la ejecucion del hilo dispatcher esta en ejecucion 
	 * una vez que el generador produzaca llamadas.	
	 */
	@Test
	public void dispatcherWaitingCallTest() throws NoEmpleyeesRegisteredExpection, 
												  NoCallRegisteredException, 
												  InterruptedException{
		callCenter.startWaitingCall();
		Thread.sleep(5000);
	    assertEquals("thread state", Thread.State.TIMED_WAITING, callCenter.dispatcherState());			
	}
	
	/**
	 * Verifica hay llamadas encoladas una vez que el generador produzca llamadas	
	 */
	@Test	
	public void queueCallHasCallTest() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(10, callCenter);
		Thread.sleep(5000);
		assertTrue("The Queue Call has not calls",CallQueue.thereAreWaitingCalls());
	}
	
	/**
	 * Verifica la atencion de 10 llamadas por todos los empleados,
	 * priorizando los operadores como los principales en la atencion
	 * de las llamdas si estos estan disponibles.
	 * 	
	 * En la salida se muestra los dialogos del dispatcher y los empleados.
	 * 
	 * Escenario: 10 llamadas generadas de manera concurrente.
	 * Se espera 5 segundos que ocurra los procesos concurrentes.
	 * 3 Operadores, 1 Supervisor y 1 Director.
	 * 
	 * Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * 1. La cola tiene llamadas a procesar
	 * 2. Los empleados estan ocupados.
	 */
	@Test	
	public void attend10CallsOn5SecsTest() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(10, callCenter);
		Thread.sleep(5000);
		assertFalse("The Queue Call has calls",CallQueue.thereAreWaitingCalls());
		assertTrue("There are Employess bussy",callCenter.allEmployeesAreFree());
	}	
	
	/**
	 * Verifica la atencion de 10 llamadas por todos los empleados,
	 * priorizando los operadores como los principales en la atencion
	 * de las llamdas si estos estan disponibles.
	 * 	
	 * En la salida se muestra los dialogos del dispatcher y los empleados.
	 * 
	 * Escenario: 10 llamadas generadas de manera concurrente.
	 * Se espera 20 segundos  que ocurra los procesos concurrentes.
	 * 3 Operadores, 1 Supervisor y 1 Director.
	 * 
	 * Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * 1. La cola tiene llamadas a procesar
	 * 2. Los empleados estan ocupados.
	 */
	@Test	
	public void attend10CallsOn20SecsTest() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(10, callCenter);
		Thread.sleep(30000);
		assertFalse("The Queue Call has calls",CallQueue.thereAreWaitingCalls());
		assertTrue("There are Employess bussy",callCenter.allEmployeesAreFree());
	}	
	
	/**
	 * Verifica la atencion de 15 llamadas por todos los empleados,
	 * priorizando los operadores como los principales en la atencion
	 * de las llamdas si estos estan disponibles.
	 * 	
	 * En la salida se muestra los dialogos del dispatcher y los empleados.
	 * 
	 * Escenario: 15 llamadas generadas de manera concurrente.
	 * Se espera 5 segundos  que ocurra los procesos concurrentes.
	 * 3 Operadores, 1 Supervisor y 1 Director.
	 * 
	 * Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * 1. La cola tiene llamadas a procesar
	 * 2. Los empleados estan ocupados.
	 */
	@Test	
	public void attendMoreThan15CallsOn5SecsTest() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(15, callCenter);
		Thread.sleep(5000);
		assertFalse("The Queue Call has calls",CallQueue.thereAreWaitingCalls());
		assertTrue("There are Employess bussy",callCenter.allEmployeesAreFree());
	}
	
	/**
	 * Verifica la atencion de mas de 15 llamadas por todos los empleados,
	 * priorizando los operadores como los principales en la atencion
	 * de las llamdas si estos estan disponibles.
	 * 	
	 * En la salida se muestra los dialogos del dispatcher y los empleados.
	 * 
	 * Escenario: 15 llamadas generadas de manera concurrente.
	 * Se espera 40 segundos  que ocurra los procesos concurrentes.
	 * 3 Operadores, 1 Supervisor y 1 Director.
	 * 
	 * Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * 1. La cola tiene llamadas a procesar
	 * 2. Los empleados estan ocupados.
	 */
	@Test	
	public void attendMoreThan15CallsOn40SecsTest() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException{
		callCenter.startWaitingCall();		
		customerCallingEmulator.start(15, callCenter);
		Thread.sleep(40000);
		assertFalse("The Queue Call has calls",CallQueue.thereAreWaitingCalls());
		assertTrue("There are Employess bussy",callCenter.allEmployeesAreFree());
	}
	
	
}
