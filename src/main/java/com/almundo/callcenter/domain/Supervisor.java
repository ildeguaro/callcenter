package com.almundo.callcenter.domain;

/**
 * Clase concreta que representa un Supervisor como Empleado a 
 * partir de la clase abtracta {@link com.almundo.callcenter.domain.Employee}.
 * <p>
 * Por defecto el metodo <code>attendCall</code> tiene la logica de interaccion
 * entre el empleado y la llamada. 
 * <p> 
 *  Se puede reescribir el metodo <code>attendCall</code> si es necesario.
 *  
 * @author  Ildenaro Medina
 */
public class Supervisor extends Employee{

	public Supervisor() {
		super();
	}
	
	public Supervisor(String name) {
		super(name);		
	}

}
