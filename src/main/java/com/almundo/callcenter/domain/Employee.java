package com.almundo.callcenter.domain;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import com.almundo.callcenter.enums.CustomerCallStatus;
import com.almundo.callcenter.enums.EmployeeStatus;
import com.almundo.callcenter.util.Logger;

/**
 * Es la clase abstracta que expone la logina basica o general
 * de la interaccion entre un empleado y la llamada a la cual se 
 * le asigno.
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public abstract class Employee extends Thread {
	
	protected EmployeeStatus status = EmployeeStatus.FREE;
	
	protected CustomerCall call;	
	
	public Employee(){
		
	}
		
	public Employee(String name){
		this.setName(name); 		
	}	
	
	@Override
	public void run() {
		while(this.status==EmployeeStatus.FREE){			
			attendCall();
		}		
	}

	/**
	 * Metodo que expone logica de interaccion entre el
	 * empleado (clase concreta) y la llamada asiganada.
	 */
	public void attendCall(){
		try {	
			if (getCall()!=null){
				this.status = EmployeeStatus.BUSSY;				
				long duration =ThreadLocalRandom.current().nextLong(5,10);
				Logger.info(this, "Efectuando llamada con "+call);
				Thread.sleep(TimeUnit.SECONDS.toMillis(duration));
				call.setDuration(duration);			
				call.setStatus(CustomerCallStatus.ATTENDED);
				Logger.info(this, "Ha finalizado llamada con "+call+". Duracion "+duration+" Segundos.");			
				this.status = EmployeeStatus.FREE;	
				Logger.info(this, "Estoy disponible...");						
				setCall(null);				
			}
				
		
		} catch (Exception e) {			
			e.printStackTrace();				
		}
	}
			
		
	public String getNameEmploye() {
		return super.getName();
	}
	
	/**
	 * Metodo que facilita la asginacion de la llamada
	 * a un empledo concreto.
	 */
	public void transferCall(CustomerCall call) {
		setCall(call);			
	}
		
	public synchronized CustomerCall getCall(){
		return this.call;		
	}
	
	public synchronized CustomerCall setCall(CustomerCall call){
		return this.call=call;		
	}

	public EmployeeStatus getStatus() {
		return status;
	}

	public void setStatus(EmployeeStatus status) {
		this.status = status;
	}
	
	@Override
	public String toString() {		
		return this.getName();
	}

}
