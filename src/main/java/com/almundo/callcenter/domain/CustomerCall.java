package com.almundo.callcenter.domain;

import java.io.Serializable;

import com.almundo.callcenter.enums.CustomerCallStatus;

/**
 * Es la clase que representa la llamada en proceso proveniente
 * del exerior del CallCenter.
 * Cuando se construye un objeto CustomerCall se inicializa 
 * en estatus WAITING
 * @see {@link CustomerCallStatus}
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com> *
 */
public class CustomerCall implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;

	private long duration;
	
	private CustomerCallStatus status;
	
	
	

	public CustomerCall(String name){
	    this.name = name;   
	    this.status = CustomerCallStatus.WAITING;
	    
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public long getDuration() {
		return duration;
	}


	public void setDuration(long duration) {
		this.duration = duration;
	}


	public CustomerCallStatus getStatus() {
		return status;
	}


	public void setStatus(CustomerCallStatus status) {
		this.status = status;
	}
	
	@Override
	public String toString() {		
		return "Llamada "+getName();
	}


}
