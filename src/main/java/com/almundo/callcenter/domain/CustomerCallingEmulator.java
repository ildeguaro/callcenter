package com.almundo.callcenter.domain;

import java.util.concurrent.ThreadLocalRandom;


/**
 * <code>CustomerCallingEmulator</code> 
 * es la clase encargada de generar n cantidad de objetos 
 * CustomerCall {@link com.almundo.callcenter.domain.CustomerCall} que 
 * representan una llamada para el CallCenter {@link com.almundo.callcenter.domain.CallCenter}}
 *  
 * @author  Ildenaro Medina
 */
public class CustomerCallingEmulator implements Runnable {	
	
	
	private CallCenter callCenter;
	
	private int maxCalls = 5;
	
	private int minxSecoundsRandom = 1;
	
	private int maxSecoundsRandom = 3;
	
	public CustomerCallingEmulator(){
		
	}
	
	@SuppressWarnings("static-access")
	public void run(){
		try {
			for (int i =1; i<=maxCalls; i++){		        
		        Thread.currentThread().sleep(ThreadLocalRandom.current()
		        		.nextLong(minxSecoundsRandom,maxSecoundsRandom)*1000);
		        this.callCenter.dial(new CustomerCall("Nro "+i));
				
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 *
	 * Registra llamadas al callCenter {@link com.almundo.callcenter.domain.CallCenter}.
	 * Ejecuta el metodo <code>run</code> de esta clase, el cual se encarga de hacer registros
	 * de manera concurrente de n cantidad de llamadas
	 * @param maxCall cantidad maxima de llamadas a emular y que se regisraran en el callcenter.
	 * @param callCenter objeto instanciado que representa el call center al cual se dirige la llamada.
	 *  
	 * @author Ildenaro Medina
	 */
	public void start(int maxCalls, CallCenter callCenter){
		this.maxCalls = maxCalls;
		this.callCenter = callCenter;		
	    new Thread(this).start();
	}

	

}
