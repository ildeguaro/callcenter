package com.almundo.callcenter.domain;

import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.List;
import com.almundo.callcenter.enums.EmployeeStatus;
import com.almundo.callcenter.exceptions.NoCallRegisteredException;
import com.almundo.callcenter.exceptions.NoEmpleyeesRegisteredExpection;
import com.almundo.callcenter.util.CallQueue;

/**
 * Es la clase encargada de recibir las llamadas que ingresan
 * por via secuencial o de manera concurrente.
 * <p> Para realizar su funcion de atender llamadas, 
 * debe estar inicializado el objeto <code>dispatcher</code>
 * que se encarga de buscar los <code>empledos</code> disponibles
 * para atender las llamadas que ingresan.
 * <p>
 * Para asegurar su correcta ejecucion, se debe registrar empleados de 
 * al menos estos tres tipos: <code>Operador</code>, <code>Supervisor</code> o
 * <code>Director</code>. Por el contrario se lanzar la excepcion:
 * <code>NoEmpleyeesRegisteredExpection</code>. * 
 *
 * @author  Ildenaro Medina
 * @see     com.almundo.callcenter.domain.Dispatcher
 * @see     com.almundo.callcenter.domain.Employee
 * @see     com.almundo.callcenter.domain.Operador
 * @see     com.almundo.callcenter.domain.Supervisor
 * @see     com.almundo.callcenter.domain.Director
 * @see     com.almundo.callcenter.exceptions.NoEmpleyeesRegisteredException
 * 
 */
public class CallCenter {	
	
	private List<Employee> employees;	
	
	private Dispatcher dispatcher;
	
	private CallQueue callQueue;
		
	public CallCenter(){
		this.employees = new ArrayList<Employee>();		
		this.dispatcher = new Dispatcher(this);
		this.callQueue = CallQueue.getInstance();
	}
	
	/**
	 *
	 * Registra empleados.
	 * @param count cantidad de empleados a registrar
	 * @param typeEmployee tipo de empleado a registrar.
	 * Ejemplo <code>Operador.class></code>	
	 * @return none	 *
	 * @author Ildenaro Medina	 
	 */
	public <T> void registerEmploy(int count, Class<T>  typeEmployee){
    	try {
    		Employee obj;
    		
	    	for (int i=1; i<=count; i++){ 
	    		typeEmployee.newInstance();
				obj = (Employee) typeEmployee.newInstance();				
	    		obj.setName(typeEmployee.getSimpleName()+" "+i);
	    		obj.start();
	    		this.employees.add(obj);
	    		
	    	}
    	} catch (InstantiationException e)  {
			e.printStackTrace();
		} catch (IllegalAccessException e){
			e.printStackTrace();
		}
    }
	
	/**
     * Cuando se ejecuta <code>dial</code> 
     * se registra la llamadas que ingresan por via secuencial
     * o concurrente la cola de llamdas <code>CallQueue</code>.     * 
     */
	@SuppressWarnings("static-access")
	public void dial(CustomerCall call){
		callQueue.queueCall(call);		
	}
	
	
	/**
     * Ejecuta el metodo <code>start</code> del objeto dispatcher {@link Dispatcher}
     */
	public void startWaitingCall() throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException{		
		if (employees.isEmpty()) 
			throw new NoEmpleyeesRegisteredExpection("No puede iniciar las labores. Debe registra Empleados");		
		this.dispatcher.start();
	}
	
	/**
     * Ejecuta el metodo <code>stop</code> del objeto dispatcher {@link Dispatcher}
     */
	public void stopDispatchCall(){
		this.dispatcher.stop();
	}
	
	/**
     * Retorna el  <code>STATUS</code> del objeto dispatcher {@link Dispatcher}
     */
	public State dispatcherState(){
		return this.dispatcher.state();
	}
	
	/**
     * Ejecuta un valor  <code>booleando</code> si todos los empleados {@link Employee}regisrados
     * estan disponibles para atender una llamada
     */
	public boolean allEmployeesAreFree(){		
		for (Employee e: employees){
			if (e.getStatus()==EmployeeStatus.BUSSY)
				return false;
		}
		return true;
	}
	
	/**
     * Retorna el empleado con mayor prioridad para tomar la llamada.
     * Debido a que puede darse el caso que un operador esta disponible 
     * antes que un supervisor o director puedan tomarla llamada, se 
     * hace una revision antes de asignar la llamada.
     * @param employee el empleado obtenido de la lista de emplados {@link Employee}
     * @return employee el empleado obtenido de la lista de emplados {@link Employee} 
     */
	public Employee priorizeAsignation(Employee employee){
	  if (employee instanceof Operador)
		  return employee;
	  
	  else if (employee instanceof Supervisor){
		  for (Employee e: employees){			 
			  if (e.getStatus()==EmployeeStatus.FREE && (e instanceof Operador ))
				  return e;
		  }
		 
	  }else if (employee instanceof Director){
		  for (Employee e: employees){			 
			  if (e.getStatus()==EmployeeStatus.FREE && (e instanceof Supervisor))
				  return e;
		  }
		  
	  }
	  return employee;
	}

	public List<Employee> getEmployees() {
		return this.employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public Dispatcher getDispatcher() {
		return dispatcher;
	}	

	@Override
	public String toString() {		
		return "Call Center Al Mundo";
	}

}
