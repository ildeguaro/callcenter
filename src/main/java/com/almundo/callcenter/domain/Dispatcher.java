package com.almundo.callcenter.domain;


import java.lang.Thread.State;

import com.almundo.callcenter.enums.CustomerCallStatus;
import com.almundo.callcenter.enums.EmployeeStatus;
import com.almundo.callcenter.util.CallQueue;
import com.almundo.callcenter.util.Logger;

/**
 * Es la clase encargada explorar la cola de llamadas
 * y asignarla a los empleados disponibles, priorizando
 * a los operadores (que se deben registrar en primer orden),
 * luego a los supervisores (que se debe registrar en 2do orden) y
 * finalmente el o los directores al final de la disposicion
 * de empleados).
 * 
 * @author  Ildenaro Medina 
 
 * @see     com.almundo.callcenter.domain.Employee
 * @see     com.almundo.callcenter.domain.Operador
 * @see     com.almundo.callcenter.domain.Supervisor
 * @see     com.almundo.callcenter.domain.Director  
 * @see     java.lang.Runnable
 */
public class Dispatcher implements Runnable{	
	
	private CallCenter callCenter;
	
	private boolean running;
	
	private Thread thread;
	
	public Dispatcher(CallCenter callCenter){
		this.callCenter = callCenter;
	}	
	
	@SuppressWarnings("static-access")
	public void run() {
		 while (running){
			Logger.info(this,"Waitng for calls...");
	        try {
	        	dispatchCall();
				Thread.currentThread().sleep(1000);				
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
	     }		 
	}
    
	/**
	 * Metodo que explora la cola de llamdas 
	 * para asignar las llamadas con status <code>WAITING</code>
	 * a un empleado (Operador, Supervisor, Director) con status
	 * <code>FREE</code>.
	 * Cuando se asigna una llamada, esta cambia a status 
	 * <code>ATTENDING</code> y al ser transferida al empleado
	 * a traves de su metodo <code>transfer<code>
	 */
    public void dispatchCall(){    	
    	CustomerCall call = CallQueue.retrieveCall();
    	if (call!=null && call.getStatus()==CustomerCallStatus.WAITING){
	    	for (Employee o: this.callCenter.getEmployees()){
	    		Logger.info(this,call+" is "+call.getStatus());	    			
    			if (o.getStatus()==EmployeeStatus.FREE){
    				call.setStatus(CustomerCallStatus.ATTENDING);
    				o = callCenter.priorizeAsignation(o);
    				o.transferCall(call);
    				CallQueue.removeCall(call);
    				break;
    			}
	    	}    	
    	}
    	
    }  
    
   
    public void start() { 
    	thread = new Thread(this); 
        running = true;
    	thread.start();
    }
    
    public State state(){
    	return this.thread.getState();
    }
    
    public void stop(){
    	this.running = false;
    }
    
    @Override
	public String toString() {		
		return "Dispatcher";
	}

}  
