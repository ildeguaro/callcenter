package com.almundo.callcenter;

import com.almundo.callcenter.domain.CallCenter;
import com.almundo.callcenter.domain.CustomerCallingEmulator;
import com.almundo.callcenter.domain.Director;
import com.almundo.callcenter.domain.Operador;
import com.almundo.callcenter.domain.Supervisor;
import com.almundo.callcenter.exceptions.NoCallRegisteredException;
import com.almundo.callcenter.exceptions.NoEmpleyeesRegisteredExpection;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public class Main {
	
	
	public static void main(String... args) throws NoEmpleyeesRegisteredExpection, NoCallRegisteredException, InterruptedException {
		CallCenter callCenter = new CallCenter();
		
		callCenter.registerEmploy(3, Operador.class);
		callCenter.registerEmploy(2, Supervisor.class);
		callCenter.registerEmploy(1, Director.class);
		
		callCenter.startWaitingCall();	
		
		new CustomerCallingEmulator().start(20,callCenter);		
					
	}

}
