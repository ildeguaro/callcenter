package com.almundo.callcenter.enums;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public enum CustomerCallStatus {
	
	WAITING, ATTENDING, ATTENDED

}
