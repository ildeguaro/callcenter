package com.almundo.callcenter.enums;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public enum EmployeeStatus {
	
	FREE, BUSSY 

}
