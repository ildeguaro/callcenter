package com.almundo.callcenter.exceptions;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public class NoEmpleyeesRegisteredExpection extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoEmpleyeesRegisteredExpection(String message) {
		super(message);
	}

}
