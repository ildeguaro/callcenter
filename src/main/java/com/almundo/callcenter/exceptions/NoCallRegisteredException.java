package com.almundo.callcenter.exceptions;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public class NoCallRegisteredException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoCallRegisteredException(String message) {
		super(message);
	}
}
