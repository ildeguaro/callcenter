package com.almundo.callcenter.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.almundo.callcenter.domain.Employee;
/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public class Logger {
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
	
	public static void info(Employee e, String s) {
	    System.out.println( "[" + formatter.format( new Date() ) + "][" + e + "]:" + s );
	}

	public static void info(Object e, String s) {
	    System.out.println( "[" + formatter.format( new Date() ) + "][" + e + "]:" + s );
	}
	
	
	public static void info(String s) {
	    System.out.println( "[" + formatter.format( new Date() ) + "]:" + s );
	}

}
