package com.almundo.callcenter.util;

import java.util.concurrent.LinkedBlockingQueue;

import com.almundo.callcenter.domain.CustomerCall;
import com.almundo.callcenter.enums.CustomerCallStatus;

/**
 * 
 * @author Ildemaro Medina <ildemaro@gmail.com>
 *
 */
public class CallQueue{

    private static CallQueue instance;

    private int counter;

    private LinkedBlockingQueue<CustomerCall> queue;
    
    public static CallQueue getInstance(){
        if ( instance == null )
            instance = new CallQueue();        
        return instance;
    }  


    private CallQueue()  {
        this.queue = new LinkedBlockingQueue<CustomerCall>();
        this.counter = 1;       
    }


    public static void queueCall(CustomerCall call) {
        try{            
            getInstance().queue.put(call);
        }
        catch ( InterruptedException e ){
           e.printStackTrace();
           
        }
    }
    
    public static boolean thereAreWaitingCalls() {    
    	boolean thereAre = false;
    	for (CustomerCall c: getInstance().queue){
    		if (c.getStatus()==CustomerCallStatus.WAITING)
    			return thereAre=true;    		
    	}    	
    	return thereAre;
       
    }
    
    public static boolean queueIsEmpty(){      	
		return getInstance().queue.isEmpty();
    }
   
    public static CustomerCall retrieveCall() {
       CustomerCall call = getInstance().queue.peek();
       return call;
    }
    
    public static void removeCall(CustomerCall call) {
        getInstance().queue.remove(call);       
    }
    
    public static boolean hasCall(){
    	return (!getInstance().queue.isEmpty());
    }
    
    public LinkedBlockingQueue<CustomerCall> getQueue() {
		return queue;
	}


	public void setQueue(LinkedBlockingQueue<CustomerCall> queue) {
		this.queue = queue;
	}


	public int getCounter() {
		return counter;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}


	

   
}
