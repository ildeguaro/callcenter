package com.almundo.callcenter.util;

import java.math.BigInteger;
import java.util.Random;

public class Names {
	
	public static String getRandomName(){
		return BigInteger.probablePrime(50, new Random()).toString(Character.MAX_RADIX).toUpperCase();

	}

}
