# Callcenter Emulator
# Autor
Ildemaro Medina
# Descripción
Callcenter es un proyecto java que emula la llegada y despacho de llamadas con procesos concurrentes. Se basa en las implementaciones de Runnable y la herencia de Thread. 
### Estructura
* **src**: Código fuente. Programa Main. 
* **test**: Unit Tests.
 
### Artefactos del Proyecto
* La Clase **CustomerCallingEmulator** es una clase que genera n llamadas de manera concurrente.
* La Clase **CallCenter** recibe las llamadas generadas de manera secuencial o concurrente. Al recibirla, se encola en objeto **CallQueue**.
* La Clase **Dispatcher** posee un proceso **runnable** que invoca el metodo _**dispatchCall**_ que esta en constante verificacion de la cola de llamadas (objeto **CallQuee**). 
Cuando **Dispatcher** encuntra una llamada en status **WAITING** y a su vez un **Empleado** (mediante una clase concreta: **Operador**, **Supervisor**, **Director**) con status **FREE** se procede a la siguiente verificacion:
  + Si el objeto **empleado** es instancia **Operador** entonces le asigna la llamada sin verificar mas condiciones. 
  + Por el contrario, si el objeto **empleado** es instancia **Supervisor** se vuelve a verificar la disponibilidad de un **operador** disponible, sino hay **operador** disponible, se procede a asginar al **empleado** **Supervisor**.
  + Si finalmente el empleado es **Director**, se procede a verificar si antes, hay un supervisor desocupado, sino lo asigna a la llamada.
 * **CallQueue** es la clase con acceso bloqueante donde **CallCenter** encola las llamadas. **Dispatcher**, en su proceso **runable** verificara si hay llamadas para se atendidas. Cuando las llamdas son asignadas a un **empleado** **Dispatcher** la remueve de la cola con el fin de no contarla como posible llamada para ser atendida. El tipo de datos que almacena **CallQueue** son de objetos **CustomerCall**.
 * **CustomerCall** es la clase que encapsula la informacion de la llamada. Para este caso: **satus**, **name** y **duration**. Este ultmo campo lo actualiza el **empleado** que atendio la llamada.
 * **Operador**, **Supervisor**, **Director** son clases concretas que heredan las propiedades y conducta de la clase abstracta **Empleado**.
 * **Empleado** clase abstracta que posee atributos y la interaccion entre una clase concreta y la llamada **CustomerCall**.

## Consideraciones
* Esta solucion atiende 10 o mas llamadas de manera concurrente. Su limite es la longitud de la cola **CallQueue** que las almacena.
* Si ingresan al **CallCenter** llamadas  y no hay **empleados** dispobibles, no importa, dado que estas permanecen en espera hasta que el **Dispatcher** encuentre un empleado disponible de acuerdo a la proridad expuesta. Para ello, las llamadas permaneceran encoladas n tiempo en **CallQuee**. No esta pautada para esta solucion la caducidad o tiempo maximo de espera para cada objeto **CustomerCall**.

### Diagrama de Clases
![Uml Class Diagram](https://bitbucket.org/ildeguaro/callcenter/downloads/uml_class_diagram_call_center.png)
### Diagrama de Secuencia
![Uml Secuence Diagram](https://bitbucket.org/ildeguaro/callcenter/downloads/uml_secuence_diagram_call_center.png)

### Testing
Considerar ejecutar **mvn package** para ejecutar los siguietes metodos tests de la clase **CallCenterTest**:

1 **printInteraction10CallsTest**
  Imprime la salida de consola de interaccion del dispatcher, empleado y la llamada. El escenario es 10 llamadas encoladas con 5 empleados (3 Operadores, 1 Supervisor y 1 Director). Hay una pausa de 7 segundos.

2 **printInteractionMoreThan10CallsTest**
 Imprime la salida de consola de interaccion del dispatcher, empleado y la llamada. El escenario es mas de 10 llamadas encoladas con 5 empleados (3 Operadores, 1 Supervisores y 1 Director). Hay una pausa de 5 segundos para evaluar el resultado.

3 **dispatcherWaitingCallTest**
  	Verificacion si el objeto **dispatcher** esta en ejecucion. Hay una pausa de 5 segundos para evaluar el resultado.

4 **queueCallHasCallTest**
  Verificacion si la cola tiene objetos llamadas encoladas. Hay una pausa de 5 segundos para evaluar el resultado.

5 **attend10CallsOn5SecsTest**
  Verifica la atencion de 10 llamadas por todos los empleados, priorizando los operadores como los principales en la atencion de las llamdas si estos estan disponibles. 	
	 En la salida se muestra los dialogos del dispatcher y los empleados. 
	 Escenario: 10 llamadas generadas de manera concurrente. Se espera 5 segundos  que ocurra los procesos concurrentes.
	 Empleados: 3 Operadores, 1 Supervisor y 1 Director. 
	 Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * La cola tiene llamadas a procesar
	 * Los empleados estan ocupados.

6 **attend10CallsOn20SecsTest**
  Verifica la atencion de 10 llamadas por todos los empleados, priorizando los operadores como los principales en la atencion de las llamdas si estos estan disponibles. 	
	 En la salida se muestra los dialogos del dispatcher y los empleados. 
	 Escenario: 10 llamadas generadas de manera concurrente. Se espera 20 segundos que ocurra los procesos concurrentes.
	 Empleados: 3 Operadores, 1 Supervisor y 1 Director. 
	 Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * La cola tiene llamadas a procesar
	 * Los empleados estan ocupados.

7 **attendMoreThan15CallsOn5SecsTest**
 Verifica la atencion de Mas de 15 llamadas por todos los empleados, priorizando los operadores como los principales en la atencion de las llamdas si estos estan disponibles. 	
	 En la salida se muestra los dialogos del dispatcher y los empleados. 
	 Escenario: 15 llamadas generadas de manera concurrente. Se espera 5 segundos que ocurra los procesos concurrentes.
	 Empleados: 3 Operadores, 1 Supervisor y 1 Director. 
	 Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * La cola tiene llamadas a procesar
	 * Los empleados estan ocupados.

8 **attendMoreThan15CallsOn40SecsTest**
 Verifica la atencion de Mas de 15 llamadas por todos los empleados, priorizando los operadores como los principales en la atencion de las llamdas si estos estan disponibles. 	
	 En la salida se muestra los dialogos del dispatcher y los empleados. 
	 Escenario: 15 llamadas generadas de manera concurrente. Se espera 40 segundos que ocurra los procesos concurrentes.
	 Empleados: 3 Operadores, 1 Supervisor y 1 Director. 
	 Resultado: Se genera 2 posibles fallas si no se cumple el test:
	 * La cola tiene llamadas a procesar
	 * Los empleados estan ocupados.

### Versiones
* Java: 1.7 (java-7-oracle)
* Junit: 4.12
* Maven: 3.0.0
